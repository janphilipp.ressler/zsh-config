# directory for oh-my-zsh
# used in:
# - ./setup.sh by oh-my-zsh install script
# - ./scripts/oh-my-zsh
export ZSH="${HOME}/.config/oh-my-zsh"

# directory for fnm (fast node manager)
# used in:
# - full-system-setup.sh
# - ./scripts/user.zsh
export FNM_PATH="${HOME}/.local/share/fnm"

