## Installation
To clone this repo use the follwing command:
```bash
git clone --recurse-submodules git@gitlab.gwdg.de:janphilipp.ressler/zsh-config.git ${HOME}/.config/zsh
```

If you've already cloned the repo and want to update the submodules inside, use this command:
```bash
git submodule update --init --recursive
```


## Activation
```
./setup.sh
```

Don't forget to reload/reboot to let `chsh` take effect.


## TODO

write update script:
- this could work as setup/install and update programm for my zsh env
- see ./todo_restructure


