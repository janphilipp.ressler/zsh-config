#!/bin/bash
set -e

# preparations
echo "[===] preparations: installing dependencies"
source /etc/os-release

case $ID in
	ubuntu) echo "This is Ubuntu!"
		sudo apt install curl zsh fzf zoxide
		;;
	fedora)
		echo "This is Fedora!"
		sudo dnf install curl zsh fzf zoxide
		;;
	*)
		echo "This is an unknown distribution."
		echo "Aborting ..."
		;;
esac


# source environment files
echo "[===] sourcing environment files"
HERE=$(cd "$(dirname "$BASH_SOURCE")"; cd -P "$(dirname "$(readlink "$BASH_SOURCE" || echo .)")"; pwd) # get the dirname of this file (for real)
source "${HERE}/zshenv.sh" # sets ZDOTDIR
source "${HERE}/env.sh" # sets ZSH and other location env variables


# check
echo "[===] checking initilization"
if [ "${HERE}" != "${ZDOTDIR}" ]; then
        echo "You are not setting up from ${ZDOTDIR}, but from ${HERE}. Are u sure you want to continue?"
        echo "If so, you should change the variable ZDOTDIR to ${HERE} in ${HERE}/zshenv."
        exit 1
fi


# initialize
echo "[===] initializing symbolic links"
function install_symlink() {
        SOURCE="$1"
        TARGET="$2"
        
        if [ ! -L "${TARGET}" ] && [ -e "${TARGET}" ]; then
                echo "${TARGET} is not a symlink. Creating backup..."
                mv -vf "${TARGET}" "${TARGET}-backup-$(date +%Y%m%d-%H%M%S)"
        fi
        
        rm -f "${TARGET}"
        
        SOURCE="$(realpath -e -L "${SOURCE}")"
        TARGET="$(realpath  -L "${TARGET}")"
        
        ln -v -s -f  "${SOURCE}" "${TARGET}"
}

install_symlink "${ZDOTDIR}/zshenv.sh" "${HOME}/.zshenv"
install_symlink "${ZDOTDIR}/zshrc.sh" "${ZDOTDIR}/.zshrc"


# installing oh-my-zsh
echo "[===] installing oh-my-zsh"
curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh | sh -s -- --unattended --keep-zshrc


# copy fonts
echo "[===] copying fonts"
FONTDIR="${HOME}/.local/share/fonts"
mkdir -p "${FONTDIR}"
cp -r "${ZDOTDIR}/fonts/MesloLGS NF" "${FONTDIR}"
echo "Hint: check that your terminal uses this font"


# changing shell
echo "[===] changing shell to zsh"
chsh -s $(which zsh)

