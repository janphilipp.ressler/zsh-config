# User configuration

# ========== meta ==========
# reload zsh config
alias :r="source ${ZDOTDIR}/.zshrc"


# ========== stuff ==========
# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

EDITOR="vim"
BROWSER="firefox"


# ========== bash path extensions: PATH ==========
# info for bash's test
# -d FILE
#   FILE exists and is a directory
# -e FILE
#   FILE exists
# -f FILE
#   FILE exists and is a regular file

# normal source path extensions e.g. directories with scripts
function add_to_PATH ()  {
    TO_INSERT=$1
    if [ -d ${TO_INSERT} ]; then
        export PATH=${TO_INSERT}:$PATH
        # echo "${TO_INSERT} is now in \$PATH"
    else
        echo "${TO_INSERT} can't be added to PATH. Not a directory or does not exist."
    fi
}

PERSONAL_LOCAL="${HOME}/data/local"

# add_to_PATH $HOME/.local/bin
add_to_PATH ${PERSONAL_LOCAL}/bin
# add_to_PATH $HOME/projects/installation
# add_to_PATH $HOME/projects/progcontests/aoc
# add_to_PATH $HOME/projects/progcontests/codeforces/cf_v1.0.0_linux_64

function execute_if_exist () {
    SCRIPT=$1
    if [ -f $SCRIPT ]; then source $SCRIPT; fi
}

# add the smartape scripts to the PATH
# TODO ...
execute_if_exist $HOME/projects/smape/smartape-aufgaben-tools/init.sh


# ========== python path extensions: PYTHONPATH ==========
# export PYTHONPATH=$HOME/bin/lib/myownpythonmodules:$PYTHONPATH
MY_PYTHON_MODULES="${PERSONAL_LOCAL}/lib/python"
[ -f ${MY_PYTHON_MODULES}/myUtils.py ] && export PYTHONPATH=${MY_PYTHON_MODULES}:$PYTHONPATH
# TODO why is this not invoking set -e problems?


# ========== java path extensions: CLASSPATH ==========
# java class path extensions
export CLASSPATH=.:$HOME/local/lib/java/stdlib.jar:$CLASSPATH


# ========== cat and/or batcat ==========
# export BAT_PAGER="less -R"
# export BAT_THEME="TwoDark"
export BAT_THEME="Monokai Extended"
# Possible values:
# * default: enables recommended style components (default).
# * full: enables all available components.
# * auto: same as 'default', unless the output is piped.
# * plain: disables all available components.
# * changes: show Git modification markers.
# * header: alias for 'header-filename'.
# * header-filename: show filenames before the content.
# * header-filesize: show file sizes before the content.
# * grid: vertical/horizontal lines to separate side bar
#         and the header from the content.
# * rule: horizontal lines to delimit files.
# * numbers: show line numbers in the side bar.
# * snip: draw separation lines between distinct line ranges.
export BAT_STYLE="changes,numbers,header-filename,header-filesize,rule,snip"

## cat aliases
# the following distinction is because the executable command provided by
# ubuntus package is called batcat and not bat (because of name conflicts??)
if ! command -v bat 2>&1 >/dev/null; then
	if ! command -v batcat 2>&1 >/dev/null; then
		echo "Problem discovered: Neither 'bat' nor 'batcat' are available commands"
	else
		alias bat="batcat"
	fi
fi

# alias cat="bat"
# alias cta="cat"
# alias pcat="cat --style=numbers,rule"

alias s="bat"

if command -v lesspipe 2>&1 >/dev/null; then
	function bat-lesspipe () {
		# tmpfile=$(mktemp "/tmp/bat-lesspipe.function.XXXXXXXX")
		# lesspipe $1 > "${tmpfile}"
		
		tmpvar="$(lesspipe "$1")"

	    	if [[ -z "${tmpvar}" ]]; then
			bat $@
	    	else
			shift
			bat <(echo "$tmpvar") $@
	    	fi
	}
	alias s="bat-lesspipe"
fi

# ========== kubectl ==========
if command -v kubectl 2>&1 >/dev/null; then
    source <(kubectl completion zsh)
    alias k="kubectl"
fi

# ========== fnm ==========
# fnm - fast node manager
test -v FNM_PATH # set in env.sh
if [ -d "$FNM_PATH" ]; then
  export PATH="${FNM_PATH}:$PATH"
  eval "`fnm env --shell zsh`"
  eval "`fnm completions --shell zsh`"
fi


# ========== ls aliases ==========
alias ls="ls --group-directories-first --color=tty"

function cdl() { builtin cd -- "$@" && { [ "$PS1" = "" ] || ls -lhrt --color; }; }
alias cl='cdl'
function cdt() { builtin cd -- "$@" && { [ "$PS1" = "" ] || tree -L 3; }; }
alias ct='cdt'

alias l='ls -lF'

alias ll='ls -alF'

alias la='ls -AlF'
alias lr='la -r'
alias lR='la -R'
alias li='la -i'


# ========== ipython3 ==========
alias ipy='ipython3'
alias ip3='ipython3'
alias calc='ipython3 --ipython-dir="${ZDOTDIR}/ipython" --profile calc'
alias calcrc='${EDITOR} ${ZDOTDIR}/ipython/profile_calc/ipython_config.py'


# ========== algos aliases ==========
alias algos="g++ -x c++ -std=c++23 -Wall -O2 -static -pipe"
alias algos20="g++ -x c++ -std=c++20 -Wall -O2 -static -pipe"
alias algosf="algos -Wfatal-errors"
alias algosd="g++ -x c++ -std=c++23 -Wall -g"

function compile_and_execute () {
    algos $1 && ./a.out
}

alias algose="compile_and_execute"


# ========== various aliases ==========
# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.

alias de='setxkbmap de'
alias us='setxkbmap us'

alias rm='rm -i'
alias mycal='cal -B 2 -A 3'

# myOwnAlias
alias zshrc='$EDITOR ${ZDOTDIR}/.zshrc'
alias userzshrc='$EDITOR ${ZDOTDIR}/scripts/user.zsh'
alias vimrc='$EDITOR ~/.vim/vimrc'

alias gs='git status'

alias sshuni='ssh -Y janphilipp.ressler@shell.stud.informatik.uni-goettingen.de'
alias sftpuni='sftp janphilipp.ressler@shell.stud.informatik.uni-goettingen.de'

alias office='/usr/lib/libreoffice/program/soffice'


# ========== WTF? ==========
alias ta='taskAnalyzer.py --no-task --todo'


# ========== stuff I don't understand ... ==========

# "sharkdp/fd" file finder, modern replacement for GNU find
# export FD_OPTIONS="--hidden --follow --ignore-file $DOTFILES/.ignore"

# 'BurntSushi/ripgrep', modern replacement for grep
# export RIPGREP_CONFIG_PATH="$DOTFILES/.ripgrep"


# REVIEW_BASE points to upstream ref during code review
# use by commands in zsh/git.sh and when viewing diffs in a Vim
# export REVIEW_UPSTREAM="develop"

# used by "oh-my-zsh/plugins/nvm"
# export NVM_DIR="$HOME/.nvm"

# Location of Tmux plugin manager
# export TMUX_PLUGIN_MANAGER_PATH="$DOTFILES/vendor/tpm"

# 'junegunn/fzf', command line fuzzy finder
# export FZF_DEFAULT_OPTS="--no-mouse --height 60% --reverse --multi --info=inline --preview='lesspipe {}' --preview-window='right:60%:wrap' --bind='f2:toggle-preview,f3:execute(bat --style=numbers {} || less -f {}),f4:execute($EDITOR {}),alt-w:toggle-preview-wrap,ctrl-d:half-page-down,ctrl-u:half-page-up,ctrl-y:execute-silent(echo {+} | pbcopy),ctrl-x:execute(rm -i {+})+abort,ctrl-l:clear-query'"
# export FZF_DEFAULT_COMMAND="git ls-files --cached --others --exclude-standard 2>/dev/null || fd --type f --type l $FD_OPTIONS"
# export FZF_CTRL_T_COMMAND="$FZF_DEFAULT_COMMAND"
# export FZF_ALT_C_COMMAND="fd --type d $FD_OPTIONS"
# export MANPATH="/usr/local/share/fzf/man:$MANPATH"


