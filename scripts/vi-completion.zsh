# enable vi-mode
# this is used instead of the default emacs-mode. Thus ctrl-p, ctrl-o and ctrl-n won't work anymore.
set -o vi

# TODO look for other nice options and mappings for vi-mode

