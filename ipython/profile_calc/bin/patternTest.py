def generate_patternTestFunc(p: str):
    def func(word: str):
        m = re.match(p, word)
        res = m.groups() if m else bool(m)
        print(res)
        return m
    return func

print("Try the new 'generate_patternTestFunc()'")

