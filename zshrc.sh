#!/bin/zsh
set -e

# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.config/zsh/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

# ========== set environment variables ==========
source "${ZDOTDIR}/env.sh" # set environment variables

# ========== run all my .zsh files ==========
# for config (${ZDOTDIR}/scripts/*.zsh) source "$config"
source "${ZDOTDIR}/scripts/oh-my-zsh.zsh"
source "${ZDOTDIR}/scripts/user.zsh"
source "${ZDOTDIR}/scripts/local.zsh"
# source "${ZDOTDIR}/scripts/vi-completion.zsh"
source "${ZDOTDIR}/scripts/tokens.zsh"
source "${ZDOTDIR}/scripts/fun-prompt.zsh"

# To customize prompt, run `p10k configure` or edit ~/.config/zsh/.p10k.zsh.
[[ ! -f "${ZDOTDIR}/.p10k.zsh" ]] || source "${ZDOTDIR}/.p10k.zsh"

# as mentioned in the documentation:
# zsh-syntax-highlighting should be sourced at the end of zshrc
# https://github.com/zsh-users/zsh-syntax-highlighting/blob/master/INSTALL.md
source "${ZDOTDIR}/custom/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh"

set +e
