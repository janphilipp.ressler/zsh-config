#!/bin/bash

remove_dir_if_exist () { [[ ! -d "$1" ]] || rm -rf "$1"; }
remove_dir_if_exist "${ZDOTDIR}" # this directory
remove_dir_if_exist "${ZSH}" # oh-my-zsh directory

remove_file_if_exist () { [[ ! -f "$1" && ! -L "$1" ]] || rm -f "$1"; }
remove_file_if_exist "${HOME}/.zshenv"
remove_file_if_exist "${HOME}/.zsh_history"
remove_file_if_exist "${HOME}/.zprofile"
